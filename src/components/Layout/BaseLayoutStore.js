import {observable, action, decorate} from 'mobx';

class BaseLayoutWrapper {
    menuCollapsed = false;

    toggleMenuCollapsed() {
        this.menuCollapsed = !this.menuCollapsed;
    } 
}

const BaseLayoutStore = decorate(BaseLayoutWrapper, {
    menuCollapsed: observable,
    toggleMenuCollapsed: action.bound
});

export default new BaseLayoutStore();