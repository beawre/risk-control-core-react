import React from "react";
import { Card } from "antd";
import { ItemPanel, Item } from "gg-editor";
import "./FlowItemPanel.css";

const FlowItemPanelArc = items => {
  return (
    <ItemPanel className="itemPanel">
      <Card bordered={false}>
        {items.items.map(x => (
          <Item
            type={x.type}
            size={x.size}
            shape={x.shape}
            model={{
              color: x.model.color,
              label: x.model.label
            }}
            src={x.src}
          />
        ))}
      </Card>
    </ItemPanel>
  );
};

export default FlowItemPanelArc;
