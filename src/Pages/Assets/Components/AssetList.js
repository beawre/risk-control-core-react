import React from "react";
import { withTranslation } from "react-i18next";
import AnalysisStatus from "./../../../components/AnalysisStatus";
import { Table, Button } from "antd";
import { Link } from "react-router-dom";

class AssetList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      nodes: props.nodes
    };
  }

  render() {
    const columns = [
      {
        title: this.props.t("Assets.table.component"),
        dataIndex: "name",
        key: "name"
      },
      {
        title: this.props.t("Assets.table.analysisStatus"),
        dataIndex: "analysis",
        key: "analysis",
        render: (value, row, index) => (
          <span>
            <AnalysisStatus key={"vulnerabilities"} title={"vulnerabilities"} count={this.state.nodes[index].vulnerabilities.length} />
            <AnalysisStatus key={"risks"} title={"risks"} count={this.state.nodes[index].risks.length} />
            <AnalysisStatus key={"treatments"} title={"treatments"} count={this.state.nodes[index].treatments.length} />
          </span>
        )
      },
      {
        title: this.props.t("Assets.table.actions"),
        key: "actions",
        render: (text, record) => (
          <Link to={`/assets/analysis/${record.id}`}>
            <Button type="primary" icon="fund">
              {this.props.t("Assets.goToAnalysis")}
            </Button>
          </Link>
        )
      }
    ];
    return <Table columns={columns} dataSource={this.state.nodes.map(node => ({ ...node, key: node.id }))} />;
  }
}

export default withTranslation()(AssetList);
